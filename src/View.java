/**
 * Class what stores String constants
 * and method what helping to showing result
 * @autor Yaroslav Chervatiuk
 * version 1.0
 */
public class View {
    /**
     * String constants what's needs to showing result for users
     */
    public static final String FIRST_NAME = "Enter the first name = ";
    public static final String EXAMPLE_FIRST_NAME = "Example (Valentina, Yaroslav)";
    public static final String LAST_NAME = "Enter the last name = ";
    public static final String EXAMPLE_LAST_NAME = "Example (Copper, Salvidor)";
    public static final String PHONE_NUMBER = "Enter phone number = ";
    public static final String EXAMPLE_PHONE_NUMBER = "Example (+380_98_543_67_43)";
    public static final String EMAIL = "Enter email = ";
    public static final String EXAMPLE_EMAIL = "Example (somename@email.net)";
    public static final String WRONG_INPUT_DATA = "Wrong input! Repeat please!";

    /**
     * Method what print a message, using a View class constants
     * @param message - a message about result of adding new information
     */
    public void printMessage(String message) {
        System.out.println(message);
    }

}
