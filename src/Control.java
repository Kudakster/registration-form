import data.Entry;

import java.util.Scanner;

/**
 * Control class, what receive information and
 * verify it and then send to model class, also
 * using View class to display the validation
 * result.
 *
 * @autor Yaroslav Chervatiuk
 * version 1.0
 */
public class Control {

    /**
     * New instances what creating by constructor
     */
    private Model model;
    private View view;

    /**
     * Constructor - a new instance of Control
     * @param model - class that saves information
     * @param view - class that store some CONSTANT STRING
     *             for showing results
     */
    public Control(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    /**
     * Regex for validation the information
     */
    public static final String REGEX_FIRST_NAME = "[A-Z][a-z]+";
    public static final String REGEX_LAST_NAME = "[A-Z][a-z]+";
    public static final String REGEX_PHONE_NUMBER = "\\+380_\\d{2}_\\d{3}_\\d{2}_\\d{2}";
    public static final String REGEX_EMAIL = "[a-z]+[A-Z]*[0-9]*-*_*@[a-z]*mail\\.[a-z]{2,6}";

    /**
     * A Method that buffers the correct information
     * and then adds to list of Entry in {@link Model} class
     */
    public void writeToRegister() {
        Scanner scanner = new Scanner(System.in);
        String firstName = checkCorrectFirstName(scanner);
        String lastName = checkCorrectLastName(scanner);
        String phoneNumber = checkCorrectPhoneNumber(scanner);
        String email = checkCorrectEmail(scanner);

        model.addToEntryList(new Entry(firstName, lastName, phoneNumber, email));

        view.printMessage(model.getEntryList().toString());
    }

    /**
     * A Method that checks the "First Name"
     * using the regex {@link Control#REGEX_FIRST_NAME}
     * @return correct "First Name"
     */
    private String checkCorrectFirstName(Scanner scanner) {
        view.printMessage(View.FIRST_NAME + "\n"
                + View.EXAMPLE_FIRST_NAME);
        while (!scanner.hasNext(REGEX_FIRST_NAME)) {
            view.printMessage(View.WRONG_INPUT_DATA + "\n"
                    + View.EXAMPLE_FIRST_NAME);
            scanner.next();
        }
        return scanner.next();
    }

    /**
     * A Method that checks the "Last Name"
     * using the regex {@link Control#REGEX_LAST_NAME}
     * @return correct "Last Name"
     */
    private String checkCorrectLastName(Scanner scanner) {
        view.printMessage(View.LAST_NAME + "\n"
                + View.EXAMPLE_LAST_NAME);
        while (!scanner.hasNext(REGEX_LAST_NAME)) {
            view.printMessage(View.WRONG_INPUT_DATA + "\n"
                    + View.EXAMPLE_LAST_NAME);
            scanner.next();
        }
        return scanner.next();
    }

    /**
     * A Method that checks the "Phone Number"
     * using the regex {@link Control#REGEX_PHONE_NUMBER}
     * @return correct "Phone Number"
     */
    private String checkCorrectPhoneNumber(Scanner scanner) {
        view.printMessage(View.PHONE_NUMBER + "\n"
                + View.EXAMPLE_PHONE_NUMBER);
        while (!scanner.hasNext(REGEX_PHONE_NUMBER)) {
            view.printMessage(View.WRONG_INPUT_DATA + "\n"
                    + View.EXAMPLE_PHONE_NUMBER);
            scanner.next();
        }
        return scanner.next();
    }

    /**
     * A Method that checks the "Email"
     * using the regex {@link Control#REGEX_EMAIL}
     * @return correct "Email"
     */
    private String checkCorrectEmail(Scanner scanner) {
        view.printMessage(View.EMAIL + "\n"
                + View.EXAMPLE_EMAIL);
        while (!scanner.hasNext(REGEX_EMAIL)) {
            view.printMessage(View.WRONG_INPUT_DATA + "\n"
                    + View.EXAMPLE_EMAIL);
            scanner.next();
        }
        return scanner.next();
    }
}
