/**
 * Main class what launch the program
 * @autor Yaroslav Chervatiuk
 * version 1.0
 */

public class Main {
    public static void main(String[] args) {
        Control control = new Control(new Model(), new View());
        control.writeToRegister();
    }
}
