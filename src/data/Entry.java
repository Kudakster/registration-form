package data;

/**
 * Class what described entry(register)
 * @autor Yaroslav Chervatiuk
 * version 1.0
 */
public class Entry {

    /**
     * Fields what described entry
     */
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;

    /**
     * Constructor - a new instance with some value
     * @param firstName - format (Lii)
     * @param lastName - format (Cooper)
     * @param phoneNumber - format (+380_111_11_11)
     * @param email - format (somename@email.net)
     */
    public Entry(String firstName, String lastName, String phoneNumber, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    /**
     * Return a string what contains all fields
     * what describe an entry
     * @return string of entry
     */
    @Override
    public String toString() {
        return "Entry{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
