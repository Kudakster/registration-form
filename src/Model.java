import data.Entry;

import java.util.LinkedList;
import java.util.List;

/**
 * Class that stores an entries in the list
 * @autor Yaroslav Chervatiuk
 * version 1.0
 */
public class Model {

    /**
     * List what stores an entries
     */
    private List<Entry> entryList = new LinkedList<>();

    /**
     * Method that adds new entry to {@link Model#entryList}
     * @param entry - instance what having a specific fields
     */
    public void addToEntryList(Entry entry) {
        entryList.add(entry);
    }

    /**
     * Method a getting field {@link Model#entryList}
     * @return entryList - list what containing entries
     */
    public List<Entry> getEntryList() {
        return entryList;
    }
}
